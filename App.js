import React, { Component } from 'react';
import { Platform, StyleSheet, SafeAreaView, View, Text, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: 'https://www.coronabuz.com/',
        };
    }

    componentWillUnmount() {}

    onReceived(notification) {}

    onOpened(openResult) {}

    onIds(device) {}

    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                    <WebView
                        ref={myWeb => (this.refWeb = myWeb)}
                        onNavigationStateChange={this.onNavigationStateChange}
                        source={{
                            uri: this.state.source,
                        }}
                        onLoadEnd={syntheticEvent => {
                            const { nativeEvent } = syntheticEvent;
                            this.setState({ canGoBack: nativeEvent.canGoBack });
                        }}
                        startInLoadingState={true}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        textZoom={100}
                        contentInset={{ bottom: 44 }}
                        //   injectedJavaScript={`(function(){ let wv = window.localStorage.getItem('webview');
                        //   if(!wv){
                        //     window.localStorage.setItem('webview', 'true');
                        //     window.location.reload();
                        //   }
                        // })();`}
                    />
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? 25 : 0,
    },
});
